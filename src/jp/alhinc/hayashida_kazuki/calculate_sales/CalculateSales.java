package jp.alhinc.hayashida_kazuki.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;


public class CalculateSales {


	public static void main(String[] args) {

		Map<String, String> branchMap = new HashMap<>();
		Map<String, Long> saleMap = new HashMap<>();

		if (!inputBranch(args[0], branchMap, saleMap)) {
			return;
		}
		if (!calculate(args[0], branchMap, saleMap)) {
			return;
		}
		output(args[0], branchMap, saleMap);
	}



	// 支店定義ファイルの読み込み
	static boolean inputBranch(String args, Map<String, String> branchMap, Map<String, Long> saleMap) {
		// ファイル名を取得
		File inputBranchFile = new File(args, "branch.lst");
		// エラー処理1.1
		// ファイルが存在しなければエラー処理
		if (!inputBranchFile.exists()) {
			System.out.println("支店定義ファイルが存在しません");
			return false;
		}

		BufferedReader br = null;
		try {
			// バッファにデータを渡す
			br = new BufferedReader(new FileReader(inputBranchFile));

			// 支店定義ファイルを1行ずつ読み込む
			String line;
			while((line = br.readLine()) != null) {
				// カンマ区切りでitems配列に格納
				String[] items = line.split(",");
				// エラー処理1.2
				// 支店コードが数値3文字でない場合、カンマで区切られた値が２個でないの場合エラー処理
				if(!(items[0].matches("[0-9]{3}")) || items.length != 2) {
					System.out.println("支店定義ファイルのフォーマットが不正です");
					return false;
				}
				// branchMapに支店コード、支店名、saleMapに支店コードと売上高(初期値0)を追加
				branchMap.put(items[0], items[1]);
				saleMap.put(items[0], 0L);
			}
		} catch(Exception e) {
			System.out.println("予期せぬエラーが発生しました");
			return false;
		}  finally {
			if(br != null) {
				try {
					br.close();
				} catch(IOException e) {
					System.out.println("予期せぬエラーが発生しました");
					return false;
				}
			}
		}
		return true;
	}



	// 集計
	static boolean calculate(String args, Map<String, String> branchMap, Map<String, Long> saleMap) {
		BufferedReader br = null;
		try {
			// 指定のファイルを取得するためのフィルター
			FilenameFilter filter = new FilenameFilter() {
				public boolean accept(File file, String str) {
					// ファイル名が"数字8桁.rcd"になっている文字列
					return str.matches("[0-9]{8}.rcd");
				}
			};

			// ディレクトリのパスをdirに格納
			File dir = new File(args);
			// filterでtrueのものだけをlist配列に格納
			File[] inputSaleList = dir.listFiles(filter);

			// list配列(ファイル名)を昇順で並び替え(エラー処理用)
			Arrays.sort(inputSaleList);

			for(int i = 0; i < inputSaleList.length; i++) {
				// エラー処理2.1
				// 売上ファイルの二つ目以降は一つ前のファイル名と比較して連番(差分が1)になっていなければエラー処理
				if ( i > 0) {
					int a = Integer.parseInt(inputSaleList[i].getName().substring(0, 8));
					int b = Integer.parseInt(inputSaleList[i-1].getName().substring(0, 8));
					if (a - b != 1) {
						System.out.println("売上ファイル名が連番になっていません");
						return false;
					}
				}

				br = new BufferedReader(new FileReader(inputSaleList[i]));

				String code = br.readLine();
				String amount = br.readLine();
				if (br.readLine() != null) {
					System.out.println(inputSaleList[i].getName() + "のフォーマットが不正です");
					return false;
				}

				// 支店コードがbranchMapのキーになければエラー処理
				if (!branchMap.containsKey(code)) {
					System.out.println(inputSaleList[i].getName() + "の支店コードが不正です");
					return false;
				}

				// 数値以外の場合エラー処理
				if (!amount.matches("[0-9]{1,}")) {
					System.out.println("予期せぬエラーが発生しました");
					return false;
				}

				Long sale = saleMap.get(code) + Long.parseLong(amount);
				if(sale.toString().length() > 10) {
					System.out.println("合計金額が10桁を超えました");
					return false;
				}
				saleMap.put(code, sale);
			}
		} catch(Exception e) {
			System.out.println("予期せぬエラーが発生しました");
			return false;
		}  finally {
			if(br != null) {
				try {
					br.close();
				} catch(IOException e) {
					System.out.println("予期せぬエラーが発生しました");
					return false;
				}
			}
		}
		return true;
	}



	// 集計結果出力
	static void output(String args, Map<String, String> branchMap, Map<String, Long> saleMap) {
		BufferedWriter bw = null;
		try {
			File outputFile = new File(args, "branch.out");
			bw = new BufferedWriter(new FileWriter(outputFile));
			// 拡張for文(branchMapのキーを全て取得し出力
			for(String key : branchMap.keySet()) {
				bw.write(key + "," + branchMap.get(key) + "," + saleMap.get(key));
				bw.newLine();
			}
		} catch(Exception e) {
			return;

		} finally {
			if(bw != null) {
				try {
					bw.close();
				} catch(IOException e) {
					System.out.println("予期せぬエラーが発生しました");
					return;
				}
			}
		}
	}


}
